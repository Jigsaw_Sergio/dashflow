import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import firebase from '../../api/firebaseApi'

export default class TableRow extends Component {
	constructor(props) {
		super(props);
		this.state = {
			currentClass: null,
			checkboxes: {
				processed: false,
				invoiced: false,
				charged: false,
				charge_failed: false,
				qb_entered: false,
				paperwork_recieved: false
			},
			notes : {
				processed: null,
				invoiced: null,
				charged: null,
				charge_failed: null,
				qb_entered: null,
				paperwork_recieved: null,
				charged_amount: null,
			},
			timestamps : {
				processed: null,
				invoiced: null,
				charged: null,
				charge_failed: null,
				qb_entered: null,
				paperwork_recieved: null,
				charged_amount: null,
			},
			firebaseListener_checkboxes: null,
			firebaseListener_a_m_duration: null,
			firebaseListener_notes: null,
			firebaseListener_timestamps: null,
			actualDurationOverride: null,
      currentJobData: null,
		};
		this.defaultDateTimeFormat = "MM-DD-YYYY @ hh:mm:ss a"
		this.updateFirebaseVal = this.updateFirebaseVal.bind(this)
		this.editNote = this.editNote.bind(this);

	}
	
	static defaultProps = {
		tvMode: false,
		jobData: {},
	};
	
	static propTypes = {
		tvMode: PropTypes.bool.isRequired,
		jobData: PropTypes.object,
	};
	
	formatJobTime(datetime) {
		return moment(datetime).format("hh:mm a")
	}

	getStatusClasses(jobStatus, jobStart, jobEnd) {
		if(jobStatus === null || jobStart === null || jobEnd === null)
			return '';
		
		if(jobStatus === "assigned" && (moment(jobStart) < moment()))
			return 'late';

		if(jobStatus === "completed")
			return 'complete';

		if(jobStatus === "started" && moment(jobEnd) < moment())
			return 'overtime';
		else if(jobStatus === "started")
			return 'started';

	}
	
	componentDidMount() {
		console.log("tableRow - componentDidMount()")

		const checkboxRef = firebase.database().ref(`/vwork_jobs/${this.props.jobData._id}/checkboxes`);
		checkboxRef.on('value', (snapshot) => {
			let firebaseCheckboxes = Object.assign({ processed: false, invoiced: false, charge_failed: false, charged: false, qb_entered: false, paperwork_recieved: false }, snapshot.val());
			let currentCheckboxes = this.state.checkboxes;
			currentCheckboxes.invoiced = firebaseCheckboxes.invoiced || false;
			currentCheckboxes.charged = firebaseCheckboxes.charged || false;
			currentCheckboxes.charge_failed = firebaseCheckboxes.charge_failed || false;
			currentCheckboxes.qb_entered = firebaseCheckboxes.qb_entered || false;
			currentCheckboxes.paperwork_recieved = firebaseCheckboxes.paperwork_recieved || false;
			currentCheckboxes.processed = firebaseCheckboxes.processed || false;
			this.setState({
				checkboxes: currentCheckboxes
			})
		})

		const a_m_durationRef = firebase.database().ref(`/vwork_jobs/${this.props.jobData._id}/actual_manual_duration`)
		a_m_durationRef.on('value', (snapshot) => {
			let theValue = snapshot.val() || null;
			this.setState({
				actualDurationOverride: theValue
			})
		})
		
		const noteRef = firebase.database().ref(`/vwork_jobs/${this.props.jobData._id}/notes`);
		noteRef.on('value', (snapshot) => {
			let firebaseCheckboxes = Object.assign({ processed: null, invoiced: null, charge_failed: null, charged: null, charged_amount: null, qb_entered: null, paperwork_recieved: null }, snapshot.val());
			let currentNotes = this.state.notes;
			currentNotes.invoiced = firebaseCheckboxes.invoiced || null;
			currentNotes.charged = firebaseCheckboxes.charged || null;
			currentNotes.charge_failed = firebaseCheckboxes.charge_failed || null;
			currentNotes.qb_entered = firebaseCheckboxes.qb_entered || null;
			currentNotes.paperwork_recieved = firebaseCheckboxes.paperwork_recieved || null;
			currentNotes.processed = firebaseCheckboxes.processed || null;
			currentNotes.charged_amount = firebaseCheckboxes.charged_amount || null;
			this.setState({
				notes: currentNotes
			})
		})
		
		const timeStampRef = firebase.database().ref(`/vwork_jobs/${this.props.jobData._id}/timestamps`);
		timeStampRef.on('value', (snapshot) => {
			let firebaseCheckboxes = Object.assign({ processed: null, invoiced: null, charge_failed: null, charged: null, charged_amount: null, qb_entered: null, paperwork_recieved: null }, snapshot.val());
			let currentTimestamps = this.state.timestamps;
			currentTimestamps.invoiced = firebaseCheckboxes.invoiced || null;
			currentTimestamps.charged = firebaseCheckboxes.charged || null;
			currentTimestamps.charge_failed = firebaseCheckboxes.charge_failed || null;
			currentTimestamps.qb_entered = firebaseCheckboxes.qb_entered || null;
			currentTimestamps.paperwork_recieved = firebaseCheckboxes.paperwork_recieved || null;
			currentTimestamps.processed = firebaseCheckboxes.processed || null;
			currentTimestamps.charged_amount = firebaseCheckboxes.charged_amount || null;
			this.setState({
				timestamps: currentTimestamps
			})
		})
		
		var tmpClass = this.getStatusClasses(this.props.jobData.status, this.props.jobData.planned_start_at, this.props.jobData.planned_end_at);
		this.setState({
			currentClass: tmpClass,
			firebaseListener_checkboxes: checkboxRef,
			firebaseListener_a_m_duration: a_m_durationRef,
			firebaseListener_notes: noteRef,
			firebaseListener_timestamps: timeStampRef,
		}, () => {
			console.log("Done!")
		})
	}

	componentWillUnmount() {
		console.log("tableRow - componentWillUnmount()")

		if(this.state.firebaseListener_checkboxes !== null)
		{
			this.state.firebaseListener_checkboxes.off();
			this.setState({
				firebaseListener_checkboxes: null
			})
		}

		if(this.state.firebaseListener_a_m_duration !== null)
		{
			this.state.firebaseListener_a_m_duration.off();
			this.setState({
				firebaseListener_a_m_duration: null
			})
		}
		
		if(this.state.firebaseListener_notes !== null)
		{
			this.state.firebaseListener_notes.off();
			this.setState({
				firebaseListener_notes: null
			})
		}
		
		if(this.state.firebaseListener_timestamps !== null)
		{
			this.state.firebaseListener_timestamps.off();
			this.setState({
				firebaseListener_timestamps: null
			})
		}
	}

	componentWillReceiveProps(nextProps) {
		var newClass = this.getStatusClasses(nextProps.jobData.status, nextProps.jobData.planned_start_at, nextProps.jobData.planned_end_at);
		this.setState({
			currentClass: newClass,
      currentJobData: nextProps.jobData,
		})
	}

	updateFirebaseVal(event) {
		const target = event.target;
		const value = target.type === 'checkbox' ? target.checked : target.value;
		const name = target.name;
		var currentBoxes = this.state.checkboxes;
		currentBoxes[name] = value
		var timestamps = this.state.timestamps;
		if(name === 'charged' && value === true){
			var chargedAmt = prompt("Enter the charged amount CAREFULLY! (DO NOT enter text, NUMBERS ONLY, you will enter an OPTIONAL message after this step)")
			chargedAmt = parseFloat(chargedAmt)
			console.log("Charged amount => " + chargedAmt);
			var checkboxes = this.state.checkboxes;
			var currentDate = moment().startOf('day').unix(); //I know, don't trust the client but we're on time constraints...
			var currentDateTime = moment().unix();
			if(isNaN(chargedAmt))
			{
				checkboxes.charged = false;
				this.setState({
					'checkboxes': checkboxes,
				})
				this.props.toastHandler.error("NUMBERS PLEASE :| You can enter a message afterwards!!!")
			} else {
				var memo = prompt("Enter your note/message for others regarding this charged amount (optional)")
				console.log("Note for everyone => " + memo)
				var currentToast = this.props.toastHandler("Submitting... please wait", { autoClose: false })
				return firebase.database().ref(`/vwork_jobs/${this.props.jobData._id}/checkboxes/${name}`).set(value)
          .then( () => {
            return firebase.database().ref(`/vwork_jobs/${this.props.jobData._id}/notes/${name}`).set(memo)
          }).then( () => {
            return firebase.database().ref(`/vwork_jobs/${this.props.jobData._id}/notes/charged_amount`).set(chargedAmt)
          }).then(() => {
            return firebase.database().ref(`/vwork_jobs/${this.props.jobData._id}/timestamps/${name}`).set(currentDateTime).then( () => {
              timestamps.charged = currentDateTime;
              var notes = this.state.notes;
              notes.charged = memo;
              notes.charged_amount = chargedAmt;
              return new Promise((resolve) => {
                this.setState({ 'timestamps': timestamps, 'checkboxes': currentBoxes, 'notes': notes }, () => {
                    resolve();
                });
              })
            })
          }).then(() => {
            let finalObject = {charged_amount: chargedAmt, jobId: this.props.jobData._id}
            return firebase.database().ref(`/dashflow_app/charge_events/${currentDate}/${currentDateTime}`).set(finalObject).then( () => {
              this.props.toastHandler.update(currentToast, {render: "Your charge has been recorded successfully!", type: this.props.toastHandler.TYPE.SUCCESS ,autoClose: 1250})
            })
          }).catch(err => {
            console.log(err)
            alert(err.message)
          })
			}
		} else if(name === 'charged' && value === false) {
			firebase.database().ref(`/vwork_jobs/${this.props.jobData._id}/checkboxes/${name}`).set(value).then( () => {
				this.setState({
					checkboxes: currentBoxes
				})
				firebase.database().ref(`/vwork_jobs/${this.props.jobData._id}/timestamps/${name}`).once("value").then( snapshot => {
					var currentDateTime = moment(snapshot.val() * 1000).unix();
					var currentDate = moment(currentDateTime * 1000).startOf('day').unix();
					console.log('currentDateTime', currentDateTime)
					console.log('currentDate', currentDate)
					timestamps.charged = null;
					firebase.database().ref(`/dashflow_app/charge_events/${currentDate}/${currentDateTime}`).remove().then(() => {
						firebase.database().ref(`/vwork_jobs/${this.props.jobData._id}/timestamps/${name}`).remove().then(() => {
							this.setState({
								'timestamps': timestamps,
							})
							this.props.toastHandler.info("You unmarked a charge!")
						})
					})
				})
			}).catch(err => {
				console.log(err)
				alert(err.message)
			})
		} else {
			firebase.database().ref(`/vwork_jobs/${this.props.jobData._id}/checkboxes/${name}`).set(value).then( () => {
				this.setState({
					checkboxes: currentBoxes
				})
				if(value === false)
				{
					firebase.database().ref(`/vwork_jobs/${this.props.jobData._id}/timestamps/${name}`).once("value").then( snapshot => {
						var currentDateTime = moment(snapshot.val() * 1000).unix();
						var currentDate = moment(currentDateTime * 1000).startOf('day').unix();
						console.log('currentDateTime', currentDateTime)
						console.log('currentDate', currentDate)
						timestamps[name] = null;
						firebase.database().ref(`/dashflow_app/${name}_events/${currentDate}/${currentDateTime}`).remove().then(() => {
							firebase.database().ref(`/vwork_jobs/${this.props.jobData._id}/timestamps/${name}`).remove().then(() => {
								this.setState({
									'timestamps': timestamps,
								})
								this.props.toastHandler.warn("You unchecked a box!", {autoClose: 2500, position: this.props.toastHandler.POSITION.BOTTOM_CENTER})
							})
						});
					});
				} else {
					var currentDateTime = moment().unix();
					var currentDate = moment(currentDateTime * 1000).startOf('day').unix();
					timestamps[name] = currentDateTime;
					firebase.database().ref(`/vwork_jobs/${this.props.jobData._id}/timestamps/${name}`).set(currentDateTime).then( () => {
						this.setState({
							'timestamps': timestamps,
						})
						let finalObject = {jobId: this.props.jobData._id}
						firebase.database().ref(`/dashflow_app/${name}_events/${currentDate}/${currentDateTime}`).set(finalObject).then(() => {
							this.props.toastHandler.success("You've checked a box!", {autoClose: 2500, position: this.props.toastHandler.POSITION.BOTTOM_CENTER})
						})
						
					})
				}
			}).catch(err => {
				console.log(err)
				alert(err.message)
			})
		}
	}
	
	formatStatusName(statusName) {
		if(statusName === 'completed' && (this.state.currentClass === 'complete' || this.state.currentClass === 'complete_tv') && (moment(this.props.jobData.actual_start_at) > moment())) {
			return 'completed'
		} else {
			return statusName;
		}
	}

	formatDuration(durationAsSecs) {
		var durationObj = moment.duration(durationAsSecs, 'seconds')
		var durationStr = {
			'hrs': durationObj.hours(),
			'mins': durationObj.minutes(),
			'secs': durationObj.seconds()
		}
		if(durationStr.hrs < 0 || durationStr.mins < 0 || durationStr.secs < 0)
			durationStr.polarity = '+'
		else
			durationStr.polarity = '-'

		durationStr.hrs = Math.abs(durationStr.hrs)
		durationStr.mins = Math.abs(durationStr.mins)
		durationStr.secs = Math.abs(durationStr.secs)
		var finalStr = "";
		var shortStr = "";

		if(durationStr.hrs !== 0) {
			finalStr += durationStr.hrs + ' hour' + (durationStr.hrs !== 1 ? 's ' : ' ')
			shortStr += durationStr.hrs + ' hour' + (durationStr.hrs !== 1 ? 's ' : ' ')
		}

		if(durationStr.mins !== 0) {
			finalStr += durationStr.mins + ' minute' + (durationStr.mins !== 1 ? 's ' : ' ')
			shortStr += durationStr.mins + ' minute' + (durationStr.mins !== 1 ? 's ' : ' ')
		}

		if(durationStr.secs !== 0) {
			finalStr += durationStr.secs + ' second' + (durationStr.secs !== 1 ? 's ' : ' ')
		}

		return {'polarity': durationStr.polarity, 'message': finalStr, 'shortMessage': shortStr};
	}


	isOvertime(jobData) {
		return (jobData.status === "started" && (moment(jobData.planned_end_at) < moment()))
	}

	isLate(jobData) {
		return (jobData.status === "assigned" && (moment(jobData.planned_start_at) < moment()))
	}

	editNote(event, name) {
		console.log(`Handle this! ${this.props.jobData._id} - ${name}`, event)
		var currentNoteObject = this.state.notes;
		var newNote = prompt("Modify note text (previous note displayed)", (currentNoteObject[name] !== null ? currentNoteObject[name] : ''))
		if(newNote !== null)
		{
			firebase.database().ref(`/vwork_jobs/${this.props.jobData._id}/notes/${name}`).set(newNote).then(() => {
				currentNoteObject[name] = (newNote === '' ? null : newNote);
				this.setState({
					notes: currentNoteObject
				})
				this.props.toastHandler.success("Your edit has been submitted!")
			});
		}
	}
	
	render() {
		const theClass = this.state.currentClass;
    var JobData = this.state.currentJobData;
    if(JobData === null)
      JobData = this.props.jobData;
    
    //console.log("Rendering " + JobData._id + " for " + JobData.planned_start_at);
		var statusName = this.formatStatusName(JobData.status);
		var statusClass = '';
		var statusSuffix = '';
		if(statusName === 'completed') {
			statusSuffix = this.formatDuration( (JobData.planned_duration - JobData.actual_duration) ).polarity === '+' ? ' (over)' : ' (under)'
		} else if(this.isOvertime(JobData)) {
			statusClass = "overtime_txt"
			statusName = "Overdue!"
		} else if(this.isLate(JobData)) {
			statusClass = "late_txt"
			statusName = "Late!"
		}

		var jsxFields = {
			actualStart: '',
			actualEnd: '',
			plannedDuration: '',
			actualDuration: '',
			discrepancyDuration: '',
			address : '',
			createdBy: '',
			createdAt: '',
			jobType: '',
		}

		if(JobData.actual_start_at && (JobData.status === 'started' || JobData.status === 'completed'))
			jsxFields.actualStart = `Actual Start: ${moment(JobData.actual_start_at).format('MM-DD-YYYY @ hh:mm:ss a')}`;

		if(JobData.actual_end_at && JobData.status === 'completed')
			jsxFields.actualEnd = `Actual Stop: ${moment(JobData.actual_end_at).format('MM-DD-YYYY @ hh:mm:ss a')}`;

		if(JobData.planned_duration)
			jsxFields.plannedDuration = `Planned Duration: ${this.formatDuration(JobData.planned_duration).message}`

		if(JobData.actual_duration && JobData.status === 'completed')
			jsxFields.actualDuration = `Actual Duration: ${this.formatDuration(JobData.actual_duration).message}`

		if(JobData.planned_duration && JobData.actual_duration)
			jsxFields.discrepancyDuration = `Discrepancy: ${this.formatDuration( (JobData.planned_duration - JobData.actual_duration) ).message + ' ' + ( this.formatDuration( (JobData.planned_duration - JobData.actual_duration) ).polarity === '+' ? '(over)' : '(under)')}`

		if(JobData.steps)
			jsxFields.address = `Address: ${JobData.steps[0].location.formatted_address}`

		if(JobData.created_by_name)
			jsxFields.createdBy = `Created by: ${JobData.created_by_name}`

		if(JobData.created_at)
			jsxFields.createdAt = `Created at: ${JobData.created_at}`
		
		if(this.state.actualDurationOverride || JobData.actual_duration)
			jsxFields.totalHours = "Hours worked (from " + (this.state.actualDurationOverride !== null ? "flowboard" : "vwork") + "): " + (this.state.actualDurationOverride !== null ? this.state.actualDurationOverride : this.formatDuration(JobData.actual_duration).shortMessage) + (this.state.actualDurationOverride !== null ? ' hour(s)' : '')

		if(JobData.jobType)
		{
			if(JobData.jobType === 'Time + Material')
				jsxFields.jobType = 'T+M';
			else if(JobData.jobType === 'Fixed Bid')
				jsxFields.jobType = 'FixedBid';
			else
				jsxFields.jobType = JobData.jobType;
		} else
			jsxFields.jobType = "N/A";

		return (
			<tr className={theClass}>
				<td className="dropdown">
					<a target="_blank" href={`https://go.vworkapp.com/html_client/jobs/${JobData.id}/edit`} style={{'textDecoration': 'none', 'color': '#34495e'}}>
						<p>{JobData.customer_name}</p>
					</a>
					<div className="dropdown-content">
						<p>{jsxFields.address}</p>
						<p>{jsxFields.createdBy}</p>
						<p>{jsxFields.createdAt}</p>
					</div>
				</td>
				<td className="dropdown">{JobData.worker_name}</td>
				<td className="dropdown">{this.formatJobTime(JobData.planned_start_at)}</td>
				<td className="dropdown">{this.formatJobTime(JobData.planned_end_at)}</td>
				<td className="dropdown">{jsxFields.jobType}</td>
				<td className="dropdown">
					<p className={statusClass}>{statusName + statusSuffix}</p>
					<div className="dropdown-content">
						<p>Planned Start: { moment(JobData.planned_start_at).format('MM-DD-YYYY @ hh:mm:ss a') }</p>
						<p>Planned Stop: { moment(JobData.planned_end_at).format('MM-DD-YYYY @ hh:mm:ss a') }</p>
						<p>{jsxFields.actualStart}</p>
						<p>{jsxFields.actualEnd}</p>
						<p>{jsxFields.plannedDuration}</p>
						<p>{jsxFields.actualDuration}</p>
						<p>{jsxFields.discrepancyDuration}</p>
						<p>{jsxFields.totalHours}</p>
					</div>
				</td>
				<td className="dropdown">
					<div className="pretty p-icon p-curve p-pulse">
						<input name="paperwork_recieved" type="checkbox" checked={this.state.checkboxes.paperwork_recieved} onChange={this.updateFirebaseVal} disabled={!this.props.enableEditing}/>
						<div className="state p-warning">
							<i className="icon mdi mdi-close"></i>
							<label></label>
						</div>
					</div>
					<div className="editIcon">
						<img src="edit.svg" alt="edit_icon"></img>
						<button onClick={(e) => this.editNote(e, 'paperwork_recieved')}>.</button>
					</div>
					<div className='dropdown-content'>
						<p>Note(s): {this.state.notes.paperwork_recieved === null ? 'N/A' : this.state.notes.paperwork_recieved}</p>
						<p>Updated at: {this.state.timestamps.paperwork_recieved === null ? 'N/A' : moment(this.state.timestamps.paperwork_recieved * 1000).format(this.defaultDateTimeFormat)}</p>
					</div>
				</td>
				<td className="dropdown">
					<div className="pretty p-icon p-curve p-pulse">
						<input name="qb_entered" type="checkbox" checked={this.state.checkboxes.qb_entered} onChange={this.updateFirebaseVal} disabled={!this.props.enableEditing}/>
						<div className="state p-warning">
							<i className="icon mdi mdi-close"></i>
							<label></label>
						</div>
					</div>
					<div className="editIcon">
						<img src="edit.svg" alt="edit_icon"></img>
						<button onClick={(e) => this.editNote(e, 'qb_entered')}>.</button>
					</div>
					<div className='dropdown-content'>
						<p>Note(s): {this.state.notes.qb_entered === null ? 'N/A' : this.state.notes.qb_entered}</p>
						
						<p>Updated at: {this.state.timestamps.qb_entered === null ? 'N/A' : moment(this.state.timestamps.qb_entered * 1000).format(this.defaultDateTimeFormat)}</p>
					</div>
				</td>
				<td className="dropdown">
					<div className="pretty p-icon p-curve p-pulse">
						<input name="charged" type="checkbox" checked={this.state.checkboxes.charged} onChange={this.updateFirebaseVal} disabled={!this.props.enableEditing}/>
						<div className="state p-warning">
							<i className="icon mdi mdi-close"></i>
							<label></label>
						</div>
					</div>
					<div className="editIcon">
						<img src="edit.svg" alt="edit_icon"></img>
						<button onClick={(e) => this.editNote(e, 'charged')}>.</button>
					</div>
					<div className='dropdown-content'>
						<p>Charged: {this.state.notes.charged_amount !== null ? ( this.state.checkboxes.charged === true  ? ('$' + this.state.notes.charged_amount + '\n') : 'No' ) : 'No'}</p>
						<p>{jsxFields.totalHours}</p>
						<p>Note(s): {this.state.notes.charged === null ? 'N/A' : this.state.notes.charged}</p>
						<p>Updated at: {this.state.timestamps.charged === null ? 'N/A' : moment(this.state.timestamps.charged * 1000).format(this.defaultDateTimeFormat)}</p>
					</div>
					<div className={this.state.notes.charged_amount !== null ? ( this.state.checkboxes.charged === true  ? 'chargeAmount' : 'chargeAmount hide' ) : 'chargeAmount hide'}>
						{this.state.notes.charged_amount !== null ? ( this.state.checkboxes.charged === true  ? ('$' + this.state.notes.charged_amount + '\n') : 'No' ) : 'No'}
					</div>
				</td>
				<td className="dropdown">
					<div className="pretty p-icon p-curve p-pulse">
						<input name="charge_failed" type="checkbox" checked={this.state.checkboxes.charge_failed} onChange={this.updateFirebaseVal} disabled={!this.props.enableEditing}/>
						<div className="state p-warning">
							<i className="icon mdi mdi-close"></i>
							<label></label>
						</div>
					</div>
					<div className="editIcon">
						<img src="edit.svg" alt="edit_icon"></img>
						<button onClick={(e) => this.editNote(e, 'charge_failed')}>.</button>
					</div>
					<div className='dropdown-content'>
						<p>Note(s): {this.state.notes.charge_failed === null ? 'N/A' : this.state.notes.charge_failed}</p>
						<p>Updated at: {this.state.timestamps.charge_failed === null ? 'N/A' : moment(this.state.timestamps.charge_failed * 1000).format(this.defaultDateTimeFormat)}</p>
					</div>
				</td>
				<td className="dropdown">
					<div className="pretty p-icon p-curve p-pulse">
						<input name="invoiced" type="checkbox" checked={this.state.checkboxes.invoiced} onChange={this.updateFirebaseVal} disabled={!this.props.enableEditing}/>
						<div className="state p-warning">
							<i className="icon mdi mdi-close"></i>
							<label></label>
						</div>
					</div>
					<div className="editIcon">
						<img src="edit.svg" alt="edit_icon"></img>
						<button onClick={(e) => this.editNote(e, 'invoiced')}>.</button>
					</div>
					<div className='dropdown-content'>
						<p>Note(s): {this.state.notes.invoiced === null ? 'N/A' : this.state.notes.invoiced}</p>
						<p>Updated at: {this.state.timestamps.invoiced === null ? 'N/A' : moment(this.state.timestamps.invoiced * 1000).format(this.defaultDateTimeFormat)}</p>
					</div>
				</td>
				<td className="dropdown">
					<div className="pretty p-icon p-curve p-pulse">
						<input name="processed" type="checkbox" checked={this.state.checkboxes.processed} onChange={this.updateFirebaseVal} disabled={!this.props.enableEditing}/>
						<div className="state p-warning">
							<i className="icon mdi mdi-close"></i>
							<label></label>
						</div>
					</div>
					<div className="editIcon">
						<img src="edit.svg" alt="edit_icon"></img>
						<button onClick={(e) => this.editNote(e, 'processed')}>.</button>
					</div>
					<div className='dropdown-content' style={{'right': '0.25vw'}}>
						<p>Note(s): {this.state.notes.processed === null ? 'N/A' : this.state.notes.processed}</p>
						<p>Updated at: {this.state.timestamps.processed === null ? 'N/A' : moment(this.state.timestamps.processed * 1000).format(this.defaultDateTimeFormat)}</p>
					</div>
				</td>
			</tr>
		);
	}
}