import React, { Component } from 'react';
import PropTypes from 'prop-types';
import TableRow from './subcomponent/tableRow.js'
import './styles/table.css';
import firebase from '../api/firebaseApi'
import axios from 'axios';
import moment from 'moment';
import createDeepstream from 'deepstream.io-client-js';

export default class TableManager extends Component {
  constructor(props) {
    super(props);
    this.state = {
      connectionState: 'INITIAL',
      renderOrder: [],
      lastUpdate: moment(),
    }
    this.ds = createDeepstream('wss://deepstream.brhapps.com');
        this.ds.on( 'connectionStateChanged', this.handleConnectionState.bind(this));
        this.ds.on('error', error => console.error(error));

    this.client = this.ds.login();
    this.record = this.client.record.getRecord('flowboard_activity')

    this.record.subscribe(value => {
      if(value.secret.forceReload) {
        window.location.reload(true)
      }
    })
    this.firebaseState = false;
    this.deepstreamState = true;
    this.dsListeners = [];
    this.tmpJobList = {};
    this.tvModeInterval = null;
    this.versionCheckInterval = null;
    this.currentTVOrder = [];
    this.deepstreamConnectionMessage = null;

    this.handleChange = this.handleChange.bind(this);
    this.timer = this.timer.bind(this);
    this.generateRows = this.generateRows.bind(this);
    this.checkVersion = this.checkVersion.bind(this);
    this.getJobDataByDate = this.getJobDataByDate.bind(this);
    this.handleNotifications = this.handleNotifications.bind(this);
    this.rerenderTheGodDamnList = this.rerenderTheGodDamnList.bind(this);
  }

  handleConnectionState( connectionState ){
    if(connectionState !== "OPEN")
    {
      this.deepstreamState = false;
      if(this.deepstreamConnectionMessage === null)
        this.deepstreamConnectionMessage = this.props.toastHandler.info("Connecting to main server, please wait...", {autoClose: false})
      else
        this.props.toastHandler.update(this.deepstreamConnectionMessage, {render: "Connecting to main server, please wait...", type: this.props.toastHandler.TYPE.INFO, autoClose: false})
    } else {
      this.deepstreamState = true;
    }
    this.setState({connectionState: connectionState});
  }

  static defaultProps = {
    tvMode: false,
    notifyMe: false,
    selectedDate: moment(),
    toastHandler: null
  };
  
  static propTypes = {
    tvMode: PropTypes.bool.isRequired,
    selectedDate: PropTypes.object.isRequired,
    notifyMe: PropTypes.bool.isRequired,
  };
  
  componentDidMount() {
    console.log("tableManager - componentDidMount()")
    this.versionCheckInterval = setInterval(this.checkVersion, 3600000) //Check every hour?
    //TODO: Require a real login!
    firebase.auth().signInWithEmailAndPassword("sergio@jigsaw.work", "123456").then( () => {
      this.firebaseState = true;
      this.getJobDataByDate();
    }).catch( err => {
      console.log(err.code)
      let friendlyError = err.message + 
        '\n' + 
        "Please refresh the page or contact a developer!";
      alert(friendlyError)
    })
  }

  checkVersion() {
    if(this.props.appData)
    {
      let ApplicationData = this.props.appData
      firebase.database().ref(ApplicationData.firebaseName + "/app_metadata").once("value").then(snapshot => {
        let metaData = snapshot.val();
        let versionNumber = metaData.version;
        let updateRequired = metaData.updateRequired;
        let changeNotes = metaData.changeNotes;
        if(ApplicationData.version !== versionNumber && updateRequired === true) {
          this.props.toastHandler.error("WARNING: You using an older version of " + ApplicationData.name + " (" + ApplicationData.version + ")", {autoClose: 300000})
          this.props.toastHandler.error("Please, Press Shift + F5 to download it now!!!", {autoClose: 300000})
          this.props.toastHandler.warn("Failure to do so may yield undesirable inputs or worst case destruction to corporate data!", {autoClose: 300000})
        } else if (ApplicationData.version !== versionNumber && updateRequired === false) {
          this.props.toastHandler.success("A new version of " + ApplicationData.name + " is available for you to download!", {autoClose: 8000})
          setTimeout(function(){
            this.props.toastHandler.info("This update (" + versionNumber + ") is optional but here is what changed!", {autoClose: 15000})
            this.props.toastHandler.info(changeNotes, {autoClose: 15000})
            this.props.toastHandler.success("Press Shift + F5 to download it now!!!", {autoClose: 60000})
          }.bind(this), 8000)
        }
      })
    }
  }
  
  componentWillUnmount() {
    console.log("tableManager - componentWillUnmount()")
    clearInterval(this.tvModeInterval)
    clearInterval(this.versionCheckInterval)
  }
  
  timer() {
    if(this.props.tvMode === true)
    {
      this.currentTVOrder.splice((this.currentTVOrder.length - 1), 0, this.currentTVOrder.splice(0, 1)[0]);
      this.setState({
        renderOrder: this.currentTVOrder
      })
    }
  }
  
  getStatusClasses(jobStatus, jobStart, jobEnd) {
    if(jobStatus === null || jobStart === null || jobEnd === null)
      return '';
    
    if(jobStatus === "assigned" && (moment(jobStart) < moment()))
      return 'late';

    if(jobStatus === "completed")
      return 'complete';

    if(jobStatus === "started" && moment(jobEnd) < moment())
      return 'overtime';
    else if(jobStatus === "started")
      return 'started';

  }
  
  handleNotifications(value) {
    var newClass = this.getStatusClasses(value.status, value.planned_start_at, value.planned_end_at);
    if(newClass === 'late' && (this.props.tvMode === true || this.props.notifyMe === true))
    {
      this.props.toastHandler.error("Technician '" + value.worker_name + "' is now LATE to " + value.customer_name + "'s job!", {autoClose: false})
    } else if( newClass === 'overtime' && (this.props.tvMode === true || this.props.notifyMe === true)) {
      this.props.toastHandler.warn("Technician '" + value.worker_name + "' is now overdue at " + value.customer_name + "'s job!", {autoClose: false})
    } else if( newClass === 'started' && (this.props.tvMode === true || this.props.notifyMe === true)) {
      this.props.toastHandler.info("Technician '" + value.worker_name + "' has started " + value.customer_name + "'s job!", {autoClose: false})
    } else if( newClass === 'complete' && (this.props.tvMode === true || this.props.notifyMe === true)) {
      this.props.toastHandler.success("Technician '" + value.worker_name + "' has completed " + value.customer_name + "'s job!", {autoClose: false})
    }
  }
  
  rerenderTheGodDamnList(newVal) {
      for(let job in this.tmpJobList)
      {
        if(this.tmpJobList[job]._id === newVal._id)
        {
          this.tmpJobList[job] = newVal;
          break;
        }
      }
      
      var tableRowsSorted = this.tmpJobList.map((item, i) => {
        return item;
      }).sort((a,b) => {
        return (
          (moment(a.planned_start_at) - moment(b.planned_start_at)) || (moment(a.planned_end_at) - moment(b.planned_end_at)) || a.customer_name > b.customer_name
        );
      });
      this.currentTVOrder = tableRowsSorted;
      this.setState({ 
        renderOrder: tableRowsSorted,
      })
  }

  getJobDataByDate(forceDate) {
    this.checkVersion();
    if(this.dsListeners.length > 0)
    {
      for(let i = 0; i < this.dsListeners.length; i++)
      {
        this.dsListeners[i].discard()
      }
      this.dsListeners = [];
    }
    var dateToPick = this.props.selectedDate;
    if(forceDate)
      dateToPick = forceDate
    return axios.get(`https://api.brhapps.com/v1/List/Jobs?date=${dateToPick.format('YYYY-MM-DD')}`)
      .then(res => {
        var newData = res.data.jobMatrix;
        return Promise.all(newData.map( jobData => {
          var currentRecord = this.client.record.getRecord(`${jobData._id}`)
          currentRecord.subscribe(value => {
            //console.log("Received: " + "this.tmpJobList[" + value._id + "]", value)
            this.rerenderTheGodDamnList(value) //Temp fix, TODO: Revamp this entire website!
            this.handleNotifications(value);
            let timeSince = moment.duration(moment().diff(this.state.lastUpdate)).asSeconds();
            if(timeSince > 5){ //Has it been 5 seconds or longer?
              this.setState({lastUpdate: moment()})
            }
          })
          return currentRecord;
        })).then( allRecords => {
          //console.log("_BEFORE_", this.tmpJobList);
          this.tmpJobList = newData;
          //console.log("_AFTER_", this.tmpJobList);
          var tableRowsSorted = this.tmpJobList.map((item, i) => {
            return item;
          }).sort((a,b) => {
            return (
              (moment(a.planned_start_at) - moment(b.planned_start_at)) || (moment(a.planned_end_at) - moment(b.planned_end_at)) || a.customer_name > b.customer_name
            );
          });
          this.currentTVOrder = tableRowsSorted;
          this.dsListeners = allRecords;
          this.setState({ 
            renderOrder: tableRowsSorted,
          })

          if(this.deepstreamConnectionMessage !== null && this.deepstreamState === true && this.firebaseState === true)
            this.props.toastHandler.update(this.deepstreamConnectionMessage, {render: "Successfully loaded " + this.props.appData.name + " version " + this.props.appData.version + "!", type: this.props.toastHandler.TYPE.SUCCESS, autoClose: 3500})
          else if(this.deepstreamState === true && this.firebaseState === true)
            this.deepstreamConnectionMessage = this.props.toastHandler.success("Successfully loaded " + this.props.appData.name + " version " + this.props.appData.version + "!", {autoClose: 3500})
          else {
            if(this.deepstreamState !== true)
              this.props.toastHandler.warn("WARNING: The main server is out of sync! Please refresh!!! (Or tell a developer!)", {autoClose: false})

            if(this.firebaseState !== true)
              this.props.toastHandler.warn("WARNING: Google's Firebase is out of sync! Please refresh!!! (Or tell a developer!)", {autoClose: false})
          }
        })
      })
      .catch(err => console.log(err))
  }
  
  handleChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    console.log("tableManager - handleChange called!")
    this.setState({
      [name]: value
    })
  }
  
  componentWillReceiveProps(nextProps) {
    console.log("tableManager - componentWillReceiveProps")
    if(nextProps.selectedDate !== this.props.selectedDate)
    {
      //Clear the jobList
      this.setState({
        renderOrder: []
      })

      //Grab new data based on date
      if(this.firebaseState === true)
      {
        this.getJobDataByDate(nextProps.selectedDate)
      }
    }

    if(nextProps.tvMode === true) {
      this.tvModeInterval = setInterval(this.timer, 10000)
    } else if(nextProps.tvMode === false) {
      clearInterval(this.tvModeInterval);
    }
  }

  generateRows() {
    return this.state.renderOrder.map( (item, i) => {
      return (
        <tbody key={item._id}>
          <TableRow toastHandler={this.props.toastHandler} tvMode={this.props.tvMode} jobData={item} enableEditing={this.props.enableEditing}  />
        </tbody>
      );
    })
  }
  
  render() {
    return (
      <div id="tblHolder">
        <table>
          <thead className="header-fixed">
            <tr>
              <th className="smaller-txt">
                <p className="headr">Customer</p>
              </th>
              <th className="smaller-txt">
                <p className="headr">Tech </p>
                <p className="headr_emoji"><span role="img" aria-label="jsx-a11y/accessible-emoji">🔨</span></p>
              </th>
              <th className="smaller-txt">
                <p className="headr">Start </p>
                <p className="headr_emoji"><span role="img" aria-label="jsx-a11y/accessible-emoji">⏰</span></p>
              </th>
              <th className="smaller-txt">
                <p className="headr">End </p>
                <p className="headr_emoji"><span role="img" aria-label="jsx-a11y/accessible-emoji">⏰</span></p>
              </th>
              <th>Type</th>
              <th>Status</th>
              <th className="smaller-txt">
                <p className="headr">Paperwork submitted </p>
                <p className="headr_emoji"><span role="img" aria-label="jsx-a11y/accessible-emoji">📦</span></p>
              </th>
              <th className="smaller-txt dropdown">
                <p className="headr_emoji" style={{'margin': '0', 'padding': '0'}}>
                  <img style={{'width': '40%', 'margin': '0', 'padding': '0'}} src="quickbooks.png" alt="qb_icon"></img>
                </p>
                <div className="dropdown-content">
                  <p style={{'fontSize': '0.4em', 'lineHeight': '1em'}}>Intuit and QuickBooks are trademarks and service marks of Intuit Inc., registered in the United States and other countries. Budget Right Handyman, a subdivision of Budget Right Kitchens LTD, is not associated with or sponsored by Intuit Inc.</p>
                </div>
              </th>
              <th className="smaller-txt">
                <p className="headr">Charged </p>
                <p className="headr_emoji"><span role="img" aria-label="jsx-a11y/accessible-emoji">💳</span></p>
              </th>
              <th className="smaller-txt">
                <p className="headr">Failed Payment</p>
              </th>
              <th className="smaller-txt">
                <p className="headr">Invoiced </p>
                <p className="headr_emoji"><span role="img" aria-label="jsx-a11y/accessible-emoji">📝</span></p>
              </th>
              <th className="smaller-txt">
                <p className="headr">Processed </p>
                <p className="headr_emoji"><span role="img" aria-label="jsx-a11y/accessible-emoji">✔️</span></p>
              </th>
            </tr>
          </thead>
          {this.generateRows()}
        </table>
        <em>{this.state.connectionState === 'OPEN' ? 'Real-time fetch enabled' : 'Connecting to server...'}</em>
      </div>
    );
  }
}