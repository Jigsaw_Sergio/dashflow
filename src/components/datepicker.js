import React from 'react';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';

class DateTimePicker extends React.Component {
	constructor (props) {
		super(props)
		this.state = {};
	}
	

	
	render() {
		return (
			<DatePicker
				name="datePicked"
				selected={this.props.datePicked}
				onChange={this.props.updateTime}
				dateFormat="MM-DD-YYYY"
			/>
		);
	}
}

export default DateTimePicker;
