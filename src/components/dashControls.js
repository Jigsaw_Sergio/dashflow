import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import './styles/dashControls.css'

export default class DashControls extends Component {
	constructor (props) {
		super(props)
		this.state = {
			showControls: false,
			lastUpdate : moment().startOf('minute').format("MM-DD-YY @ hh:mm a")
		}
		this.changeControls = this.changeControls.bind(this)
	}
	
	static defaultProps = {
		tvMode: false,
		enableEditing: false,
		updateAppState: this.changeControls
	};
	
	static propTypes = {
		tvMode: PropTypes.bool.isRequired,
		updateAppState: PropTypes.func.isRequired,
		enableEditing: PropTypes.bool.isRequired,
	};
	
	changeControls(event) {
		const target = event.target;
		const value = target.type === 'checkbox' ? target.checked : target.value;
		const name = target.name;
		this.setState({
			[name]: value
		})
	}
	
	render() {
		return (
			<div className={this.state.showControls ? 'controlMenu expanded' : 'controlMenu'}>
				<button onClick={(e) => this.changeControls({'target': {'type' : 'button', 'value': !this.state.showControls, 'name': 'showControls'}})}>Show controls</button>
				<div className={!this.state.showControls ? 'hidden' : ''}>
					<button onClick={(e) => this.props.updateAppState({'target': {'type' : 'button', 'value': this.props.enableEditing, 'name': 'enableEditing'}})}>Toggle Editing</button>
					<br />
					<p>Enable TV Mode</p>
					<input name="tvMode" type="checkbox" checked={this.props.tvMode} onChange={this.props.updateAppState}/>
					<br />
					<p style={{'fontSize': '0.8em'}}>Subscribe to changes {this.props.tvMode === true ? '(receiving notifications from TV Mode)' : ''}</p>
					<input name="notifyMe" type="checkbox" checked={this.props.notifyMe || this.props.tvMode} onChange={this.props.updateAppState} disabled={this.props.tvMode}/>
					<br />
					<p>Last update: {this.state.lastUpdate}</p>
				</div>
			</div>
		);
	}
}