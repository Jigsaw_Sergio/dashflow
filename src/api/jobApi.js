import React, { Component } from 'react';
import firebase from './firebaseApi.js'

export default class FirebaseDataStore extends Component {
	constructor(props) {
		super(props);
		this.state = {
			version: null
		}
		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
		this.getVersionData = this.getVersionData.bind(this);
	}
	
	handleChange(event) {
		const target = event.target;
		const value = target.type === 'checkbox' ? target.checked : target.value;
		const name = target.name;
		console.log("FirebaseDataStore - handleChange called!")
		this.setState({
			[name]: value
		})
	}
	
	handleSubmit(e) {
		//e.preventDefault();
		const versionToSet = "2.0.0"
		const versionRef = firebase.database().ref('dashboard_app/version');
		//versionRef.set(versionToSet);
		console.log("FirebaseDataStore - handleSubmit called!")
		this.setState({
			version: versionToSet
		});
	}
	
	getVersionData() {
		return this.state.version;
	}
	
	componentDidMount() {
		console.log("FirebaseDataStore - componentDidMount()")
		const versionRef = firebase.database().ref('dashboard_app/version');
		versionRef.on('value', (snapshot) => {
			let theVal = snapshot.val();
			console.log("FirebaseDataStore - Retrieved", theVal)

			this.setState({
				version: theVal
			})
		})
	}
	
	render() {
		return (
			<div></div>
		);
	}
}