import React, { Component } from 'react';
import './App.css';
import DateTimePicker from './components/datepicker.js';
import DashControls from './components/dashControls.js';
import TableManager from './components/tableManager.js';
import moment from 'moment';
import { ToastContainer, toast } from 'react-toastify';

export default class App extends Component {
	constructor (props) {
		super(props)
		this.state = {
			tvMode: false,
			notifyMe: false,
			enableEditing: false,
			datePicked: moment(),
			appData : {
				name: "Dashflow",
				firebaseName: "dashflow_app",
				version: "1.5.1",
			}
		}
	}
	
	componentDidMount() {
		console.log("TODO: Add tvMode location param")
	}
	
	updateAppState(event) {
		const target = event.target;
		const value = target.type === 'checkbox' ? target.checked : target.value;
		const name = target.name;
		console.log("App - updateAppState()", {[name] : value})

		if(name === 'tvMode' && value === true)
			toast.success("TV Mode is now enabled!", {autoClose: 3500})
		else if(name === 'tvMode' && value === false)
			toast.error("TV Mode is now disabled!", {autoClose: 3500})

		if(name === 'notifyMe' && value === true) {
			toast.success("You have subscribed for notifcations!", {autoClose: 3500})
			toast.warn("Note: You will not recieve MOBILE notifications! Stay on this page!")
		} else if(name === 'notifyMe' && value === false)
			toast.error("You have unsubscribed, farewell for now!", {autoClose: 3500})

		if(name === 'enableEditing' && !this.state.enableEditing === true)
			toast.warn("Editing enabled! Be responsible!", {autoClose: 3500})
		else if(name === 'enableEditing' && !this.state.enableEditing === false)
			toast.success("Editing disabled! You can rest easy now.", {autoClose: 3500})

		if(target.type === 'button')
		{
			this.setState(prevState => ({
				[name]: !prevState[name]
			}));
		} else {
			this.setState({
				[name]: value
			})
		}
	}

	updateTime(newDate) {
		this.setState({
			datePicked: newDate
		})
	}
	
	render() {
		return (
			<div className="App">
				<ToastContainer autoClose={10000}  />
				<DateTimePicker datePicked={this.state.datePicked} updateTime={this.updateTime.bind(this)}/>
				<DashControls tvMode={this.state.tvMode} notifyMe={this.state.notifyMe} updateAppState={this.updateAppState.bind(this)}/>
				<TableManager toastHandler={toast} notifyMe={this.state.notifyMe} tvMode={this.state.tvMode} enableEditing={this.state.enableEditing} selectedDate={this.state.datePicked} appData={this.state.appData}/>
			</div>
		);
	}
};